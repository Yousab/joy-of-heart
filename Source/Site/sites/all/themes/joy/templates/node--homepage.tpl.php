<section id="text-carousel-intro-section" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(img/slider-bg.jpg);">

    <div class="container">
        <div class="caption text-center text-white" data-stellar-ratio="0.7">

            <div id="owl-intro-text" class="owl-carousel">
                <?php foreach ($field_hp_slides as $slide): ?>
                  <div class="item">
                      <?php // kpr($slide['entity']->title);die; ?>
                      <h1><?php print $slide['entity']->title; ?></h1>
                      <p><?php print $slide['entity']->field_slide_caption['und'][0]['value']; ?></p>
                      <div class="extra-space-l"></div>
                      <a class="btn btn-blank" href="<?php print $slide['entity']->field_slide_url['und'][0]['value']; ?>" target="_blank" role="button">View More!</a>
                  </div>
                <?php endforeach; ?>
            </div>

        </div> <!-- /.caption -->
    </div> <!-- /.container -->

</section>

<section id="about-section" class="page bg-style1">
    <!-- Begin page header-->
    <div class="page-header-wrapper">
        <div class="container">
            <div class="page-header text-center wow fadeInUp" data-wow-delay="0.3s">
                <h2>About</h2>
                <div class="devider"></div>
                <p class="subtitle">little information</p>
            </div>
        </div>
    </div>
    <!-- End page header-->

    <!-- Begin rotate box-1 -->
    <div class="rotate-box-1-wrapper">
        <div class="container">
            <div class="row">
                <?php print views_embed_view('about_us', 'block'); ?>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <!-- End rotate box-1 -->
</section>

<!-- Begin cta -->
<section id="cta-section">
    <div class="cta">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <h1><?php print $field_hp_portfolio_title[0]['value']; ?></h1>
                    <p><?php print $field_hp_portfolio_description[0]['value']; ?></p>
                </div>

                <div class="col-md-3">
                    <div class="cta-btn wow bounceInRight" data-wow-delay="0.4s">
                        <a class="btn btn-default btn-lg" href="<?php print file_create_url($field_hp_portfolio_download[0]['uri']); ?>" target="_blank" role="button">Download</a>
                    </div>
                </div>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
</section>
<!-- End cta -->