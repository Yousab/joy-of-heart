<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<?php
   if(!drupal_is_front_page()) {
     header("location: http://joy.localhost");
     //exit;
   }
?>
<div class="body">
    <header id="header" class="header-main">

        <!-- Begin Navbar -->
        <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="index.html">Unika</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="body">Home</a></li>
                        <li><a class="page-scroll" href="#about-section">About</a></li>
                        <li><a class="page-scroll" href="#services-section">Services</a></li>
                        <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                        <li><a class="page-scroll" href="#team-section">Team</a></li>
                        <li><a class="page-scroll" href="#prices-section">Prices</a></li>
                        <li><a class="page-scroll" href="#contact-section">Contact</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
        <!-- End Navbar -->

    </header>

    <div class="main-container <?php print $container_class; ?>">

        <header role="banner" id="page-header">
            <?php if (!empty($site_slogan)): ?>
              <p class="lead"><?php print $site_slogan; ?></p>
            <?php endif; ?>

            <?php print render($page['header']); ?>
        </header> <!-- /#page-header -->

        <div class="row">

            <?php if (!empty($page['sidebar_first'])): ?>
              <aside class="col-sm-3" role="complementary">
                  <?php print render($page['sidebar_first']); ?>
              </aside>  <!-- /#sidebar-first -->
            <?php endif; ?>

            <section<?php print $content_column_class; ?>>
                <?php if (!empty($page['highlighted'])): ?>
                  <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
                <?php endif; ?>
                <?php
                if (!empty($breadcrumb)): print $breadcrumb;
                endif;
                ?>
                <a id="main-content"></a>
                <?php print render($title_prefix); ?>
                <?php if (!empty($title)): ?>
                  <h1 class="page-header"><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php print $messages; ?>
                <?php if (!empty($tabs)): ?>
                  <?php print render($tabs); ?>
                <?php endif; ?>
                <?php if (!empty($page['help'])): ?>
                  <?php print render($page['help']); ?>
                <?php endif; ?>
                <?php if (!empty($action_links)): ?>
                  <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>
<?php print render($page['content']); ?>
            </section>

                <?php if (!empty($page['sidebar_second'])): ?>
              <aside class="col-sm-3" role="complementary">
              <?php print render($page['sidebar_second']); ?>
              </aside>  <!-- /#sidebar-second -->
<?php endif; ?>

        </div>
    </div>

    <div class="footer-top">
        <div class="container">
            <div class="row wow bounceInLeft" data-wow-delay="0.4s">

                <div class="col-sm-6 col-md-4">
                    <h4>Useful Links</h4>
                    <ul class="imp-links">
                        <li><a href="">About</a></li>
                        <li><a href="">Services</a></li>
                        <li><a href="">Press</a></li>
                        <li><a href="">Copyright</a></li>
                        <li><a href="">Advertise</a></li>
                        <li><a href="">Legal</a></li>
                    </ul>
                </div>

                <div class="col-sm-6 col-md-4">
                    <h4>Subscribe</h4>
                    <div id="footer_signup">
                        <div id="email">
                            <form id="subscribe" method="POST">
                                <input type="text" placeholder="Enter email address" name="email" id="address" data-validate="validate(required, email)"/>
                                <button type="submit">Submit</button>
                                <span id="result" class="section-description"></span>
                            </form>
                        </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>

                <div class="col-sm-12 col-md-4">
                    <h4>Recent Tweets</h4>
                    <div class="single-tweet">
                        <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident</div>
                        <div class="tweet-date">1 Hour ago</div>
                    </div>
                    <div class="single-tweet">
                        <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident uku shumaru</div>
                        <div class="tweet-date">1 Hour ago</div>
                    </div>
                </div>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

    <div class="footer">
        <div class="container text-center wow fadeIn" data-wow-delay="0.4s">
            <p class="copyright">Copyright &copy; 2016 - &amp; Joy of Heart. All rights reserved</p>
        </div>
    </div>
    <a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a> <!-- Scroll to top button -->

        <?php if (!empty($page['footer'])): ?>
      <footer class="footer <?php print $container_class; ?>">
      <?php print render($page['footer']); ?>
      </footer>
<?php endif; ?>
</div>
