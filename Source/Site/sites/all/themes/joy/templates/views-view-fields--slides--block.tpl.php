<div class="item">
    <h1><?php print $fields['title']->content; ?></h1>
    <p><?php print $fields['field_slide_caption']->content; ?></p>
    <div class="extra-space-l"></div>
    <a class="btn btn-blank" href="<?php print $fields['field_slide_url']->content; ?>" target="_blank" role="button">View More!</a>
</div>