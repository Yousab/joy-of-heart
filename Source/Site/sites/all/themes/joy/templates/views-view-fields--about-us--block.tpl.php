<div class="col-md-3 col-sm-6">
    <a href="#" class="rotate-box-1 square-icon wow zoomIn" data-wow-delay="0.2s">
        <span class="rotate-box-icon"><i class="<?php print $fields['field_about_fontawesome_icon']->content ?>"></i></span>
        <div class="rotate-box-info">
            <h4><?php print $fields['title']->content ?></h4>
            <p><?php print $fields['body']->content ?></p>
        </div>
    </a>
</div>