<?php

/**
 * @file
 * The primary PHP file for this theme.
 */

function joy_preprocess_page(&$variables) {
  drupal_add_js('sites/all/themes/joy/js/owl.carousel.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -10));
  drupal_add_js('sites/all/themes/joy/js/jquery.stellar.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -9));
  drupal_add_js('sites/all/themes/joy/js/wow.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -8));
  drupal_add_js('sites/all/themes/joy/js/waypoints.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -7));
  drupal_add_js('sites/all/themes/joy/js/isotope.pkgd.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -6));
  drupal_add_js('sites/all/themes/joy/js/classie.js', array('type' => 'file', 'scope' => 'footer','weight' => -5));
  drupal_add_js('sites/all/themes/joy/js/jquery.easing.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -4));
  drupal_add_js('sites/all/themes/joy/js/jquery.counterup.min.js', array('type' => 'file', 'scope' => 'footer','weight' => -3));
  drupal_add_js('sites/all/themes/joy/js/smoothscroll.js', array('type' => 'file', 'scope' => 'footer','weight' => -2));
  drupal_add_js('sites/all/themes/joy/js/theme.js', array('type' => 'file', 'scope' => 'footer','weight' => -1));
}
